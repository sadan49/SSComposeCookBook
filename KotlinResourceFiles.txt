.
├── androidviews
│   └── AndroidViews.kt
├── animation
│   ├── AnimatedHeartButton.kt
│   ├── AnimationActivity.kt
│   ├── basic
│   │   ├── ChangeColorAnimationWithState.kt
│   │   ├── RotateViewAnimation.kt
│   │   └── VisibilityAnimation.kt
│   ├── BasicAnimation.kt
│   ├── contentAnimation
│   │   ├── ContentAnimation.kt
│   │   ├── ContentIconAnimationActivity.kt
│   │   └── FabButtonWithContent.kt
│   ├── GestureAnimationActivity.kt
│   ├── Header.kt
│   ├── InfiniteTransitionActivity.kt
│   ├── ShimmerAnimationActivity.kt
│   ├── SwipeToDelete.kt
│   ├── TabBarAnimationActivity.kt
│   ├── Tabbar.kt
│   └── ui
│       └── theme
│           └── Color.kt
├── appbar
│   └── TopAppBarActivity.kt
├── bottomnav
│   ├── BottomNavigationActivity.kt
│   ├── HomeScreen.kt
│   ├── NotificationScreen.kt
│   ├── ProfileScreen.kt
│   ├── ScreenType.kt
│   ├── SearchScreen.kt
│   └── UserModel.kt
├── button
│   └── ButtonActivity.kt
├── canvas
│   ├── BasicCanvasExampleActivity.kt
│   ├── CanvasActivity.kt
│   ├── CanvasAndroidEasterEggActivity.kt
│   ├── CanvasBlendModesActivity.kt
│   ├── CanvasDrawingBoardActivity.kt
│   ├── CanvasDrawScopeOperationActivity.kt
│   ├── CanvasPathOperationActivity.kt
│   ├── CanvasPathsActivity.kt
│   ├── CanvasShapesActivity.kt
│   ├── CanvasTextImageActivity.kt
│   ├── CanvasTouchOperationActivity.kt
│   ├── CanvasUtils.kt
│   └── DrawProperties.kt
├── checkbox
│   ├── CheckBoxActivity.kt
│   └── Profession.kt
├── constraintlayout
│   ├── BarrierActivity.kt
│   ├── ChainActivity.kt
│   ├── ConstraintLayoutActivity.kt
│   └── GuidelineActivity.kt
├── data
│   └── DataProvider.kt
├── datepicker
│   └── DatePickerActivity.kt
├── demosamples
│   ├── instagramdemo
│   │   ├── components
│   │   │   ├── InstaGramPostItem.kt
│   │   │   └── InstagramStoriesItem.kt
│   │   ├── data
│   │   │   └── Items.kt
│   │   ├── InstagramHomeActivity.kt
│   │   ├── InstagramLoginActivity.kt
│   │   └── InstagramSplashActivity.kt
│   └── SampleUIActivity.kt
├── dialog
│   └── DialogActivity.kt
├── dropdownmenu
│   └── DropDownMenuActivity.kt
├── floatingactionbutton
│   └── FloatingActionButtonActivity.kt
├── imagepicker
│   └── ImagePickerActivity.kt
├── KotlinResourceFiles.txt
├── list
│   ├── advancelist
│   │   ├── model
│   │   │   └── MovieResponse.kt
│   │   ├── MovieList.kt
│   │   ├── retrofit
│   │   │   ├── MovieSource.kt
│   │   │   ├── RetrofitClient.kt
│   │   │   └── RetrofitService.kt
│   │   ├── uiclass
│   │   │   └── MovieItem.kt
│   │   └── viewmodel
│   │       └── MovieViewModel.kt
│   ├── AdvanceListActivity.kt
│   ├── LazyColumnRowActivity.kt
│   ├── LazyGridActivity.kt
│   └── LazyListActivity.kt
├── MainActivity.kt
├── maps
│   ├── basic
│   │   ├── BasicMapActivity.kt
│   │   ├── BasicMapsViewModel.kt
│   │   ├── LiteMapInListActivity.kt
│   │   └── MapInScrollingActivity.kt
│   ├── circle
│   │   ├── MapCircleActivity.kt
│   │   └── MapCircleViewModel.kt
│   ├── cluster
│   │   ├── CustomInfoWindowAdapters.kt
│   │   ├── MapClusterActivity.kt
│   │   └── SSClusterItem.kt
│   ├── geojson
│   │   └── MapGeoJsonActivity.kt
│   ├── GoogleMapsActivity.kt
│   ├── GoogleMapsComponents.kt
│   ├── GoogleMapsStates.kt
│   ├── GoogleMapsUIOptions.kt
│   ├── GoogleMapUtil.kt
│   ├── indoorlevel
│   │   └── MapIndoorActivity.kt
│   ├── interoperability
│   │   └── MapsInXMLActivity.kt
│   ├── marker
│   │   ├── MapMarkerActivity.kt
│   │   └── MapMarkerViewModel.kt
│   ├── overlay
│   │   ├── MapGroundOverlayActivity.kt
│   │   ├── MapGroundOverlayViewModel.kt
│   │   ├── MapHeatMapOverlayActivity.kt
│   │   ├── MapKMLOverlayActivity.kt
│   │   ├── MapTileOverlayActivity.kt
│   │   └── MapTileOverlayViewModel.kt
│   ├── place
│   │   ├── component
│   │   │   ├── MapPlaceScreens.kt
│   │   │   ├── MapsNavigationViewer.kt
│   │   │   └── MapsPlacePicker.kt
│   │   ├── model
│   │   │   ├── AutoCompleteResponse.kt
│   │   │   ├── DirectionResponse.kt
│   │   │   ├── PlaceResponse.kt
│   │   │   └── Resource.kt
│   │   ├── retrofit
│   │   │   ├── MapRetrofitClient.kt
│   │   │   └── MapRetrofitService.kt
│   │   ├── ui
│   │   │   ├── MapsNavigationActivity.kt
│   │   │   └── MapsPlaceActivity.kt
│   │   └── viewmodel
│   │       ├── MapsPlaceActivityViewModel.kt
│   │       ├── NavigationViewModel.kt
│   │       └── PlacePickerViewModel.kt
│   ├── polygon
│   │   ├── MapPolygonActivity.kt
│   │   └── MapPolygonViewModel.kt
│   ├── polyline
│   │   ├── MapPolylineActivity.kt
│   │   └── MapPolylineViewModel.kt
│   ├── projection
│   │   └── MapProjectionActivity.kt
│   ├── scalebar
│   │   ├── MapScaleBarActivity.kt
│   │   └── MapScaleBarViewModel.kt
│   └── snapshot
│       └── MapSnapShotActivity.kt
├── model
│   └── Component.kt
├── navigationdrawer
│   ├── BottomDrawerActivity.kt
│   ├── ModalDrawerActivity.kt
│   └── NavigationDrawerTypesActivity.kt
├── parallaxeffect
│   └── ParallaxEffectActivity.kt
├── pulltorefresh
│   ├── CustomBackgroundPullToRefreshActivity.kt
│   ├── CustomViewPullToRefreshActivity.kt
│   ├── PullToRefreshActivity.kt
│   └── SimplePullToRefresh.kt
├── radiobutton
│   └── RadioButtonActivity.kt
├── slider
│   └── SliderActivity.kt
├── snackbar
│   └── SnackBarActivity.kt
├── swipetodelete
│   ├── SwipeableItemCell.kt
│   ├── SwipeDirection.kt
│   ├── SwipeToDeleteDirectionActivity.kt
│   └── SwipeToDeleteListActivity.kt
├── switch
│   └── SwitchActivity.kt
├── tabarlayout
│   └── TabBarLayoutActivity.kt
├── textfield
│   └── TextFieldActivity.kt
├── textstyle
│   └── SimpleTextActivity.kt
├── theme
│   ├── AppThemeState.kt
│   ├── Color.kt
│   ├── Shape.kt
│   ├── SystemUI.kt
│   ├── ThemeActivity.kt
│   ├── Theme.kt
│   └── Type.kt
├── timepicker
│   └── TimePickerActivity.kt
├── viewpager
│   ├── AddRemovePagerActivity.kt
│   ├── HorizontalPagerWithFlingBehaviorActivity.kt
│   ├── HorizontalPagerWithIndicator.kt
│   ├── HorizontalViewPager.kt
│   ├── PagerExtension.kt
│   ├── SilderWithLabel.kt
│   ├── VerticalViewPagerWithIndicatorActivity.kt
│   ├── viewmodel
│   │   └── AddRemovePagerViewModel.kt
│   ├── ViewPagerActivity.kt
│   ├── ViewPagerWithSwipeAnimationActivity.kt
│   └── ViewPagerWithTabActivity.kt
├── xmls
│   ├── ComposeInXmlViews.kt
│   └── CustomComposeToolbar.kt
└── zoomview
    └── ZoomViewActivity.kt

66 directories, 161 files
